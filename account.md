# Your account

Wikibase.cloud requires a user account to use. Here's everything you need to know to create and activate your account.

## Your invite code

To create an account, you'll need an invite code.

Wikibase.cloud is in a closed beta, which means we have limited capacity to offer. If you’re interested in a Wikibase.cloud instance, fill out an application for early access [here](https://lime.wikimedia.de/index.php/717538). 

We'll consider each application on its merits, but we'll give greater consideration to projects aligned with our [movement](https://meta.wikimedia.org/wiki/Movement_Strategy) and [Linked Open Data](https://meta.wikimedia.org/wiki/LinkedOpenData/Strategy2021) strategies. We’ll open up more capacity once our beta comes to an end.

## Create an account

On the wikibase.cloud front page, click the "Get started" button on the top right to open the [account creation page](https://www.wikibase.cloud/create-account).

![](https://i.imgur.com/CohjsRc.png)

---

Enter your invite code, your email and your chosen password. Once you're agreed to the terms of use, click "Create account".

![](https://i.imgur.com/Czr4a7i.png)

---

Sucessfully submitting this form takes you to the wikibase.cloud dashboard -- but with a warning that your email address not yet verified.

![](https://i.imgur.com/dQ8Lad1.png)

## Verify your email address

Before you create a wiki, you need to verify the email address you provided. Check your email -- you should receive a message that looks like this:

![](https://i.imgur.com/KxYODyB.png)

---

Click the "Verify email" button in the email. When you load the wikibase.cloud dashboard again, you'll see that the warning has disappeared. In its place is a blue "+" button, which allows you to create your first wiki.

![](https://i.imgur.com/e3WGsBu.png)

## Reset your password

To reset your password, visit the [forgotten password page](https://wikibase.cloud/forgotten-password).
