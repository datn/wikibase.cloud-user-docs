# For users

- [Your account](./account) - Get started on wikibase.cloud. Learn how to use your invite code, set up your new account and create your first wiki.
- [Using your wiki](./wikis) - How to use your new MediaWiki instance and Wikibase.
