# Your wiki

Wikibase.cloud lets you create MediaWiki wikis with Wikibase. Here's everything you need to know about creating and managing a wiki.

## Creating a wiki

On the [dashboard page](https://www.wikibase.cloud/dashboard), click the "+" button to create a new wiki.

![](https://i.imgur.com/e3WGsBu.png)

---

Fill out the "Create a wiki" form. You can choose your own site name and initial MediaWiki user name. 

### Site domain

The site domain is what goes in the browser to navigate to your site. You have two choices for the site domain name:

* **Free subdomain**: If available, the name you choose becomes a subdomain of wikibase.cloud. It must be at least 20 characters long. Example: ***my-amazing-library-site**.wikibase.cloud*
* **Custom domain**: If you have an existing domain name you'd like to use, enter it here. In [DNS](https://en.wikipedia.org/wiki/Domain_Name_System), that name should be a CNAME pointing to: <code>sites-1.dyna.wikibase.cloud.</code>

![](https://i.imgur.com/rWJ6GuD.png)

---

After you click "Create wiki", your new wiki's settings page will appear.

::: tip
The details of the MediaWiki user account you chose will be emailed to the email address you provided.
:::

## Settings page

The settings page has two tabs: "Wiki settings" and "Features". 

### Wiki settings tab

In the settings tab you can control various aspects of your new wiki, such as the default skin and logo. You can also delete your wiki from this page.

* **Details**: This panel displays important information about your wiki for your reference.
* **Skin**: Here you can choose your wiki's skin -- your site's general appearance.
  * **Logo**: You can set the logo that appears in the top corner of every page on your wiki. Uploaded images should be square -- they will be resized into a 135-pixel by 135-pixel square.
* **Registration**: By turning this switch on you disable open registration on your wiki, requiring new accounts to be approved by users of your site who have the "[bureaucrat](https://www.mediawiki.org/wiki/Bureaucrat)" right.
* **Wikibase options**: Proceed with caution! Changing these values may cause problems with your wiki. 
  * **Value lengths**: These fields control the length (in characters) of strings and of monolingual text fields. Lengths can range from 400 (default) to 2500. 
    * **Multilang lengths**:  This field controls the length (in characters) of multilanguage fields such as labels and descriptions. Lengths can range from 250 (default) to 2500.
* **Delete site**: If you would like to delete your site, click the "Delete site" button. Be careful: deletion is irreversible! You will be asked to confirm your choice, after which your site will be deleted forever.

![](https://i.imgur.com/CXQSirF.png)

### Features tab

The features tab allows you to manage some specific Wikibase features.

- Wikibase Equivalent Entities
- [Wikibase Lexeme](./feature-lexeme)
- Wikibase Federated Properties

![](https://i.imgur.com/o7UlbmJ.png)
